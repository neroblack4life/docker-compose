# docker-compose

This project contains docker-compose files that contain GitLab and its many integrations. It was created for Support to have a fast go to for when they need to test issues that arise from tickets.

Note: The current image for `gitlab/gitlab-ee` does not yet have an arm64 ready image, so if you have an M1 MacBook - the `docker-compose up` command will not work since the image will be stuck at one point. See https://gitlab.com/groups/gitlab-org/-/epics/2370 and https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5673 for updates regarding arm64 images.

## How to run
Just clone the repo, go into the folder of the intergration and run:

```bash
docker-compose up
```

After everything is running, you will be able to go to the intergration's respective page in the browser and set it up.

For example: GitLab will be at http://localhost and Jira Software can be found at http://localhost:8080

## To stop 

```bash
# cntrl-c to stop the containers
docker-compose down
```

## Remember: 
* After you are in GitLab, you will need to first [allow requests to the local network from webhook](https://docs.gitlab.com/ee/security/webhooks.html)s before setting
up your chosen intergration.
* When connecting an intergration, you need to use IP's of the Docker network. i.e. 172.19.0.0/24
* To change the GitLab version, edit the `gitlab.image` line in the YAML file from `latest` to the [version tag](https://hub.docker.com/r/gitlab/gitlab-ee/tags/) you need.
